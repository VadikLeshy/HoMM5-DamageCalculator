#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  static double get_full_damage(double creatures, double baseDmg, double attack, double defence);

private slots:
  void calculate();

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
