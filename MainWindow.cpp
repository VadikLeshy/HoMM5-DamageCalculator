#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <math.h>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  QSpinBox *ctrls[] = {ui->spinAttack, ui->spinBaseDmg, ui->spinCount, ui->spinDefence, ui->spinHP};
  for(auto *ptr: ctrls)
    connect(ptr, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
  connect(ui->spinModifier, SIGNAL(valueChanged(double)), this, SLOT(calculate()));
}

MainWindow::~MainWindow()
{
  delete ui;
}

inline constexpr double dabs(double x)
{ return x<0? -x: x; }

double MainWindow::get_full_damage(double creatures, double baseDmg, double attack, double defence)
{
  double attackDefenceDelta = dabs(attack - defence) * 0.05;

  double k = 1.0 + attackDefenceDelta;

  if(attack < defence)
    k = 1.0 / k;
  else
    if(fabsl(attack - defence) < 0.01)
      k = 1.0;

  return creatures * baseDmg * k;
}

void MainWindow::calculate()
{
  double dmg = get_full_damage(ui->spinCount->value(), ui->spinBaseDmg->value(), ui->spinAttack->value(), ui->spinDefence->value());
  dmg *= ui->spinModifier->value();
  ui->lblDmg->setText(tr("Damage: ") + QString::number(dmg));

  int killed = floorf(dmg / ui->spinHP->value());
  ui->spinKilled->setValue(killed);
}
